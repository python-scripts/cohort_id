import sys  # provides access to variables used by the interpreter
import os  # traversing Windows directory
import pandas as pd  # data manipulation and analysis
import numpy as np  # scientific computing
from datetime import datetime  # manipulating dates and time
from tqdm import tqdm  # Customisable progressbar decorator for iterators

# add custom imports path (insert at 1, 0 is the script path)
sys.path.insert(1, r"D:\Users\nmccasland\WorkDocs\projects")

# import custom function to read local normalized files
from my_funcs import read_normalized

# set users desktop and change directory
desktop = os.path.join(os.path.join(os.environ["USERPROFILE"]), "Desktop")
os.chdir(desktop)

# silence Pandas SettingWithCopyWarning
pd.set_option("chained_assignment", None)

# set date and time variables
start_time = datetime.now().strftime("%H:%M:%S")

# print start time
print("\nStart:", datetime.now().strftime("%H:%M:%S"))

# ----------------------------------------------------------------------------


def specimen_dx_link(df_dict, dz_dict):
    """ 
    Accepts dictionary of normalized file dataframes and dictionary mapping
    M2GEN disease types to ICD-O PrimaryDiagnosisSite and HistologyCodes.
    
    Returns dataframes with matched and unmatched Specimen to Diagnosis 
    records.
    """

    print("\nPrepping dataframes...")
    # ------------------------------------------------------------------------
    # Prep ClinicalMolLinkage
    # ------------------------------------------------------------------------

    # set copy of dataframe from dictionary
    spec_df = df_dict["ClinicalMolLinkage"].copy(deep=True)

    # change column name to later join with Diagnosis
    spec_df.rename(columns={"ORIENAvatarKey": "AvatarKey"}, inplace=True)

    # filter on tumor specimen records only
    tumor_df = spec_df.loc[spec_df["Tumor/Germline"] == "Tumor"]

    # extract 5-digit HistologyCode for comparison
    tumor_df["SpHistology"] = tumor_df["Histology/Behavior"].str[0:5]

    # ------------------------------------------------------------------------
    # Prep Diagnosis
    # ------------------------------------------------------------------------

    # set copy of dataframe from dictionary
    dx_df = df_dict["Diagnosis"].copy(deep=True)

    # remove forward slash to extract 5-digit HistologyCode for comparison
    dx_df["DxHistology"] = dx_df["HistologyCode"].str.replace(r"/", "")

    print("\nJoining dataframes...")
    # ------------------------------------------------------------------------
    # Join dataframes
    # ------------------------------------------------------------------------

    # join on AvatarKey
    link_dx = pd.merge(tumor_df, dx_df, how="outer", on="AvatarKey")

    # add empty columns to identify rows with matching criteria
    link_dx["Disease_Match"] = ""
    link_dx["Histology_Match"] = ""
    link_dx["PrimarySite_Match"] = ""

    # iterate over dataframe rows
    for index, row in tqdm(
        link_dx.iterrows(),
        total=link_dx.shape[0],
        colour=("green"),
        desc="Sp-Dx Matching",
    ):
        # set row values
        disease = row["Disease Type"]
        sp_histology = row["SpHistology"]
        dx_histology = row["DxHistology"]
        primary_site = row["PrimaryDiagnosisSiteCode"]

        # iterate over nested dictionary
        for cohort, nested in dz_dict.items():
            for code, alt_code in nested.items():
                # match Disease Type with dict key
                if disease == cohort:
                    # flag row
                    link_dx.at[index, "Disease_Match"] = cohort
                    # if CodeGroup (tuple) is HistologyCode...
                    if code[0] == "HistologyCode":
                        # if no applicable histologies...
                        if code[1] == "None":
                            # flag row
                            link_dx.at[index, "Histology_Match"] = "NA"
                        # match Specimen histology to CodeGroup (tuple) value
                        elif sp_histology == code[1]:
                            # match Specimen to Diagnosis histology directly
                            if sp_histology == dx_histology:
                                # flag row
                                link_dx.at[index, "Histology_Match"] = "Exact Match"
                            # match Diagnosis to compatiable histologies
                            elif dx_histology == alt_code[0]:
                                # flag row
                                link_dx.at[index, "Histology_Match"] = "Alt Match"

                    # if CodeGroup (tuple) is PrimaryDiagnosisSiteCode...
                    if code[0] == "PrimaryDiagnosisSiteCode":
                        # if no applicable primary site codes...
                        if code[1] == "None":
                            # flag row
                            link_dx.at[index, "PrimarySite_Match"] = "NA"
                        # match primary site code to CodeGroup (tuple) value
                        elif primary_site == code[1]:
                            link_dx.at[index, "PrimarySite_Match"] = "Match"

    # replace empty strings with NaN
    link_dx.replace(r"^s*$", np.nan, regex=True, inplace=True)

    # add column to flag rows that match on disease, histology and primary
    link_dx["MatchedKey"] = np.where(
        link_dx["Disease_Match"].notna()
        & link_dx["Histology_Match"].notna()
        & link_dx["PrimarySite_Match"].notna(),
        "Yes",
        "No",
    )

    # extract 4-digit histology to later identify cohorts
    link_dx["DxHistology"] = (
        link_dx["DxHistology"].str.replace(r"/", "").astype(str).str[0:4]
    )
    link_dx["SpHistology"] = (
        link_dx["SpHistology"].str.replace(r"/", "").astype(str).str[0:4]
    )

    # create column of lists containing specimen and diagnosis histology codes
    link_dx["CohortHistology"] = link_dx[["SpHistology", "DxHistology"]].values.tolist()

    # reindex columns
    link_dx = link_dx.reindex(
        columns=[
            "AvatarKey",
            "WES",
            "RNASeq",
            "Disease Type",
            "SpecimenSiteOfOrigin",
            "SpecimenSiteOfCollection",
            "Primary/Met",
            "Histology/Behavior",
            "Age At Specimen Collection",
            "AgeAtDiagnosis",
            "PrimaryDiagnosisSiteCode",
            "PrimaryDiagnosisSite",
            "ClinGroupStage",
            "PathGroupStage",
            "HistologyCode",
            "SpHistology",
            "DxHistology",
            "Disease_Match",
            "Histology_Match",
            "PrimarySite_Match",
            "MatchedKey",
            "CohortHistology",
        ]
    )

    # split dataframes on matched and unmatched records
    matched_df = link_dx.loc[link_dx["MatchedKey"] == "Yes"]
    unmatched_df = link_dx.loc[link_dx["MatchedKey"] == "No"]

    return matched_df, unmatched_df


# ----------------------------------------------------------------------------
# Non-Hodgkin Lymphoma (NHL)
# ----------------------------------------------------------------------------

"""
Each function below accepts a dataframe of matched specimen and diagnosis
records and identifies records which meet a given cohorts disease, primary
site and histology requirements. 
"""

dz_hist_pri = "(`Disease Type` in @disease) & (SpHistology in @histology or DxHistology in @histology) & (PrimaryDiagnosisSiteCode in @primary_site)"
dz_hist = "(`Disease Type` in @disease) & (SpHistology in @histology or DxHistology in @histology)"
dz_pri = "(`Disease Type` in @disease) & (PrimaryDiagnosisSiteCode in @primary_site)"


def nhl_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["NHL"]["Disease Type"]
    histology = cohort_dict["NHL"]["HistologyCode"]

    df = matched_df.query(dz_hist)

    df.insert(1, "Cohort", "NHL")

    return df


# ----------------------------------------------------------------------------
# Hodgkin Lymphoma (HL)
# ----------------------------------------------------------------------------


def hl_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["NHL"]["Disease Type"]
    histology = cohort_dict["NHL"]["HistologyCode"]

    df = matched_df.query(dz_hist)

    df.insert(1, "Cohort", "HL")

    return df


# ----------------------------------------------------------------------------
# HEME
# ----------------------------------------------------------------------------


def heme_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["HEME"]["Disease Type"]
    histology = cohort_dict["HEME"]["HistologyCode"]

    df = matched_df.query(dz_hist)

    df.insert(1, "Cohort", "HEME")

    return df


# ----------------------------------------------------------------------------
# Mesothelioma
# ----------------------------------------------------------------------------


def meso_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["MESO"]["Disease Type"]
    histology = cohort_dict["MESO"]["HistologyCode"]

    df = matched_df.query(dz_hist)

    df.insert(1, "Cohort", "MESO")

    return df


# ----------------------------------------------------------------------------
# Blood and Bone marrow malignancies
# ----------------------------------------------------------------------------


def bbmm_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["BBMM"]["Disease Type"]
    histology = cohort_dict["BBMM"]["HistologyCode"]
    primary_site = cohort_dict["BBMM"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "BBMM")

    return df


# ----------------------------------------------------------------------------
# Pancreatic
# ----------------------------------------------------------------------------


def panc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["PANC"]["Disease Type"]
    primary_site = cohort_dict["PANC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_pri)

    df.insert(1, "Cohort", "PANC")

    return df


# ----------------------------------------------------------------------------
# Neuroendocrine pancreatic cancer (NPC)
# ----------------------------------------------------------------------------


def npc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["NPC"]["Disease Type"]
    histology = cohort_dict["NPC"]["HistologyCode"]
    primary_site = cohort_dict["NPC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "NPC")

    return df


# ----------------------------------------------------------------------------
# Exocrine pancreatic cancer (EPC)
# ----------------------------------------------------------------------------


def epc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["EPC"]["Disease Type"]
    histology = cohort_dict["EPC"]["HistologyCode"]
    primary_site = cohort_dict["EPC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "EPC")

    return df


# ----------------------------------------------------------------------------
# Glioblastoma multiforme (GBM) of the brain
# ----------------------------------------------------------------------------


def gbm_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["GBM"]["Disease Type"]
    histology = cohort_dict["GBM"]["HistologyCode"]
    primary_site = cohort_dict["GBM"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "GBM")

    return df


# ----------------------------------------------------------------------------
# Hepatocellular carcinoma (HCC)
# ----------------------------------------------------------------------------


def hcc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["HCC"]["Disease Type"]
    histology = cohort_dict["HCC"]["HistologyCode"]
    primary_site = cohort_dict["HCC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "HCC")

    return df


# ----------------------------------------------------------------------------
# Head & Neck (H&N)
# ----------------------------------------------------------------------------


def hn_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["H&N"]["Disease Type"]
    primary_site = cohort_dict["H&N"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_pri)

    df.insert(1, "Cohort", "H&N")

    return df


# ----------------------------------------------------------------------------
# Renal cell carcinoma (RCC)
# ----------------------------------------------------------------------------


def rcc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["RCC"]["Disease Type"]
    histology = cohort_dict["RCC"]["HistologyCode"]
    primary_site = cohort_dict["RCC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "RCC")

    return df


# ----------------------------------------------------------------------------
# Non-small cell lung cancer (NSCLC)
# ----------------------------------------------------------------------------


def nsclc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["NSCLC"]["Disease Type"]
    histology = cohort_dict["NSCLC"]["HistologyCode"]
    primary_site = cohort_dict["NSCLC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "NSCLC")

    return df


# ----------------------------------------------------------------------------
# Bladder
# ----------------------------------------------------------------------------


def ubc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["UBC"]["Disease Type"]
    primary_site = cohort_dict["UBC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_pri)

    df.insert(1, "Cohort", "UBC")

    return df


# ----------------------------------------------------------------------------
# Colorectal Cancer (CRC)
# ----------------------------------------------------------------------------


def crc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["CRC"]["Disease Type"]
    histology = cohort_dict["CRC"]["HistologyCode"]
    primary_site = cohort_dict["CRC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "CRC")

    return df


# ----------------------------------------------------------------------------
# Cutaneous melanoma (CM)
# ----------------------------------------------------------------------------


def cm_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["CM"]["Disease Type"]
    histology = cohort_dict["CM"]["HistologyCode"]
    primary_site = cohort_dict["CM"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "CM")

    return df


# ----------------------------------------------------------------------------
# Noncutaneous melanoma (NCM)
# ----------------------------------------------------------------------------


def ncm_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["NCM"]["Disease Type"]
    histology = cohort_dict["NCM"]["HistologyCode"]
    primary_site = cohort_dict["NCM"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "NCM")

    return df


# ----------------------------------------------------------------------------
# Sarcoma
# ----------------------------------------------------------------------------


def sar_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["SAR"]["Disease Type"]
    histology = cohort_dict["SAR"]["HistologyCode"]

    df = matched_df.query(dz_hist)

    df.insert(1, "Cohort", "SAR")

    return df


# ----------------------------------------------------------------------------
# Cutaneous squamous cell carcinoma (cSCC)
# ----------------------------------------------------------------------------


def cscc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["cSCC"]["Disease Type"]
    histology = cohort_dict["cSCC"]["HistologyCode"]
    primary_site = cohort_dict["cSCC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "cSCC")

    return df


# ----------------------------------------------------------------------------
# Merkel cell carcinoma
# ----------------------------------------------------------------------------


def mcc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["MCC"]["Disease Type"]
    histology = cohort_dict["MCC"]["HistologyCode"]
    primary_site = cohort_dict["MCC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "MCC")

    return df


# ----------------------------------------------------------------------------
# Thyroid
# ----------------------------------------------------------------------------


def thy_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["THY"]["Disease Type"]
    histology = cohort_dict["THY"]["HistologyCode"]
    primary_site = cohort_dict["THY"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "THY")

    return df


# ----------------------------------------------------------------------------
# Head & Neck SCC
# ----------------------------------------------------------------------------


def hnscc_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["HNSCC"]["Disease Type"]
    histology = cohort_dict["HNSCC"]["HistologyCode"]
    primary_site = cohort_dict["HNSCC"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "HNSCC")

    return df


# ----------------------------------------------------------------------------
# UUT
# ----------------------------------------------------------------------------


def uut_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["UUT"]["Disease Type"]
    histology = cohort_dict["UUT"]["HistologyCode"]
    primary_site = cohort_dict["UUT"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "UUT")

    return df


# ----------------------------------------------------------------------------
# AML
# ----------------------------------------------------------------------------


def aml_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["AML"]["Disease Type"]
    histology = cohort_dict["AML"]["HistologyCode"]
    primary_site = cohort_dict["AML"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "AML")

    return df


# ----------------------------------------------------------------------------
# DLBCL
# ----------------------------------------------------------------------------


def dlbcl_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["DLBCL"]["Disease Type"]
    histology = cohort_dict["DLBCL"]["HistologyCode"]

    df = matched_df.query(dz_hist)

    df.insert(1, "Cohort", "DLBCL")

    return df


# ----------------------------------------------------------------------------
# Endometrial
# ----------------------------------------------------------------------------


def endo_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["ENDO"]["Disease Type"]
    primary_site = cohort_dict["ENDO"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_pri)

    df.insert(1, "Cohort", "ENDO")

    return df


# ----------------------------------------------------------------------------
# Breast Cancer
# ----------------------------------------------------------------------------


def brst_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["BRST"]["Disease Type"]
    histology = cohort_dict["BRST"]["HistologyCode"]
    primary_site = cohort_dict["BRST"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "BRST")

    return df


# ----------------------------------------------------------------------------
# Prostate Cancer
# ----------------------------------------------------------------------------


def prst_cohort(matched_df):

    # set lists from dictionary
    disease = cohort_dict["PRST"]["Disease Type"]
    histology = cohort_dict["PRST"]["HistologyCode"]
    primary_site = cohort_dict["PRST"]["PrimaryDiagnosisSiteCode"]

    df = matched_df.query(dz_hist_pri)

    df.insert(1, "Cohort", "PRST")

    return df


"""
******************************************************************************
 GET DATA
******************************************************************************
"""

# set path to normalized files
files_path = "D:/Users/nmccasland/WorkDocs/data/normalized-files/\
Investor Release"

# read files into dictionary of dataframes
file_dict = read_normalized(files_path)

print("\nCreating dictionaries")
# set path to codes file
codes_file = "D:/Users/nmccasland/WorkDocs/projects/analysis/cohort-id/\
scripts/data-files/cohort_codes_20210112.xlsx"

# read cohort and disease codes into dataframes
cohort_df = pd.read_excel(
    codes_file,
    dtype="string",
    sheet_name="Cohort Codes",
    keep_default_na=False,
    na_values="",
)

disease_df = pd.read_excel(
    codes_file,
    dtype="string",
    sheet_name="Disease Codes",
    keep_default_na=False,
    na_values="",
)

# convert dataframes to nested dictionaries
cohort_dict = {
    k: f.groupby("CodeGroup")["Code"].apply(list).to_dict()
    for k, f in cohort_df.groupby("Cohort")
}

disease_dict = {
    k: f.groupby(["CodeGroup", "Code"])["AltCode"].apply(list).to_dict()
    for k, f in disease_df.groupby("Disease Type")
}

"""
******************************************************************************
 CALL FUNCTIONS
******************************************************************************
"""

# call function to match Specimen to Diagnosis records
(matched_specimens, unmatched_specimens) = specimen_dx_link(file_dict, disease_dict)

# copy matched dataframe
matched_dataframe = matched_specimens.copy()

# create empty list for processed dataframes from each cohort function
frames = []

# call cohort functions and add returned dataframes to list
nhl_df = nhl_cohort(matched_dataframe)
frames.append(nhl_df)

hl_df = hl_cohort(matched_dataframe)
frames.append(hl_df)

heme_df = heme_cohort(matched_dataframe)
frames.append(heme_df)

meso_df = meso_cohort(matched_dataframe)
frames.append(meso_df)

bbmm_df = bbmm_cohort(matched_dataframe)
frames.append(bbmm_df)

panc_df = panc_cohort(matched_dataframe)
frames.append(panc_df)

npc_df = npc_cohort(matched_dataframe)
frames.append(npc_df)

epc_df = epc_cohort(matched_dataframe)
frames.append(epc_df)

gbm_df = gbm_cohort(matched_dataframe)
frames.append(gbm_df)

hcc_df = hcc_cohort(matched_dataframe)
frames.append(hcc_df)

hn_df = hn_cohort(matched_dataframe)
frames.append(hn_df)

rcc_df = rcc_cohort(matched_dataframe)
frames.append(rcc_df)

nsclc_df = nsclc_cohort(matched_dataframe)
frames.append(nsclc_df)

ubc_df = ubc_cohort(matched_dataframe)
frames.append(ubc_df)

crc_df = crc_cohort(matched_dataframe)
frames.append(crc_df)

cm_df = cm_cohort(matched_dataframe)
frames.append(cm_df)

ncm_df = ncm_cohort(matched_dataframe)
frames.append(ncm_df)

sar_df = sar_cohort(matched_dataframe)
frames.append(sar_df)

cscc_df = cscc_cohort(matched_dataframe)
frames.append(cscc_df)

mcc_df = mcc_cohort(matched_dataframe)
frames.append(mcc_df)

thy_df = thy_cohort(matched_dataframe)
frames.append(thy_df)

hnscc_df = hnscc_cohort(matched_dataframe)
frames.append(hnscc_df)

uut_df = uut_cohort(matched_dataframe)
frames.append(uut_df)

aml_df = aml_cohort(matched_dataframe)
frames.append(aml_df)

dlbcl_df = dlbcl_cohort(matched_dataframe)
frames.append(dlbcl_df)

endo_df = endo_cohort(matched_dataframe)
frames.append(endo_df)

brst_df = brst_cohort(matched_dataframe)
frames.append(brst_df)

prst_df = prst_cohort(matched_dataframe)
frames.append(prst_df)

print("Concatenating dataframes...")
# concat list of returned dataframes
cohort_dataframe = pd.concat(frames, ignore_index=True, sort=False).reset_index(
    drop=True
)

# reindex columns
cohort_dataframe = cohort_dataframe.reindex(
    columns=[
        "AvatarKey",
        "Cohort",
        "WES",
        "RNASeq",
        "Disease Type",
        "SpecimenSiteOfOrigin",
        "SpecimenSiteOfCollection",
        "Primary/Met",
        "Histology/Behavior",
        "Age At Specimen Collection",
        "AgeAtDiagnosis",
        "PrimaryDiagnosisSiteCode",
        "PrimaryDiagnosisSite",
        "ClinGroupStage",
        "PathGroupStage",
        "HistologyCode",
        "SpHistology",
        "DxHistology",
        "Disease_Match",
        "Histology_Match",
        "PrimarySite_Match",
        "MatchedKey",
        "CohortHistology",
    ]
)

"""
******************************************************************************
  WRITE TO EXCEL
******************************************************************************
"""

print("\nWriting to file")
# set date string
cur_datetime = datetime.today().strftime("%Y-%m-%dT%H%M%S")

# set filename
filename = "/Spec-Dx_Link_{}.xlsx".format(cur_datetime)

# create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter(desktop + filename, engine="xlsxwriter")

# # convert the dataframe to an XlsxWriter Excel object
cohort_dataframe.to_excel(writer, sheet_name="Patient Cohorts", index=False)
matched_specimens.to_excel(writer, sheet_name="Matched Sp & Dx", index=False)
unmatched_specimens.to_excel(writer, sheet_name="Unmatched Sp & Dx", index=False)
cohort_df.to_excel(writer, sheet_name="Cohort Codes", index=False)
disease_df.to_excel(writer, sheet_name="Disease Codes", index=False)

# save and close file
writer.save()
writer.close()

# print end time
print("\nEnd:", datetime.now().strftime("%H:%M:%S"))
